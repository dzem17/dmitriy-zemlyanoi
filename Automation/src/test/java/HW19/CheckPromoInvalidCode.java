package HW19;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class CheckPromoInvalidCode {

By promo = By.xpath("//*[@id=\"root\"]/div/div/div/div/div/input");
By btnPromo = By.xpath("//*[@id=\"root\"]/div/div/div/div/div/button");
By errorMessage = By.xpath("//*[@id=\"root\"]/div/div/div/div/div/span");

    public void setPromo (String strPromo){
        $(promo).setValue(strPromo);

    }

    public void clickBtnPromo (){
        $(btnPromo).click();
    }

    public void errorMessage (){
       $(errorMessage).should(visible, text("Invalid code ..!"));

    }
    public void promo(String promostr){
        this.setPromo(promostr);
        this.clickBtnPromo();
        this.errorMessage();
    }
}
