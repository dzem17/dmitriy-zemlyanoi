package HW19;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.Random;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;

public class AddBasket {
    Random rand = new Random();
    int vegetables1 = rand.nextInt(30) + 1;
    int vegetables2 = rand.nextInt(30) + 1;
    String randvegetab1 = $(By.xpath("/html/body/div/div/div[1]/div/div[" + (vegetables1) + "]/h4")).getText();
    String randvegetab2 = $(By.xpath("/html/body/div/div/div[1]/div/div[" + (vegetables2) + "]/h4")).getText();
    SelenideElement basket1 = $(By.xpath("//*[@id=\"root\"]/div/header/div/div[3]/div[2]/div[1]/div[1]/ul/li[1]/div[1]/p[1]"));
    SelenideElement basket2 = $(By.xpath("//*[@id=\"root\"]/div/header/div/div[3]/div[2]/div[1]/div[1]/ul/li[2]/div[1]/p[1]"));
    By Checkout = By.xpath("//*[@id=\"root\"]/div/header/div/div[3]/div[2]/div[2]/button");

    public AddBasket randVegetables1() {
        $(By.xpath("/html/body/div/div/div[1]/div/div[" + (vegetables1) + "]/div[3]/button")).click();
        return this;
    }

    public AddBasket randVegetables2() {
        $(By.xpath("/html/body/div/div/div[1]/div/div[" + (vegetables2) + "]/div[3]/button")).click();
        return this;
    }

    public AddBasket openBasket() {
        $(By.xpath("//*[@id=\"root\"]/div/header/div/div[3]/a[4]/img")).click();
        return this;
    }

    public void checkText1() {
        this.basket1.shouldHave(text(randvegetab1));
    }

    public void checkText2() {
        this.basket2.shouldHave(text(randvegetab2));
    }

    public AddBasket checkoutBtn() {
        $(Checkout).click();
        return this;
    }


    public void checkText() {
        this.checkText1();
        this.checkText2();

    }
    public void sumRandVeg() {
        String priceRandVeg1 = $(By.xpath("/html/body/div[1]/div/div[1]/div/div[" + (vegetables1) + "]/p")).getText();
        String priceRandVeg2 = $(By.xpath("/html/body/div[1]/div/div[1]/div/div[" + (vegetables2) + "]/p")).getText();
        $(By.xpath("/html/body/div/div/div[1]/div/div[" + (vegetables1) + "]/div[3]/button")).click();
        $(By.xpath("/html/body/div/div/div[1]/div/div[" + (vegetables2) + "]/div[3]/button")).click();
        String priceBasket = $(By.xpath("//*[@id=\"root\"]/div/header/div/div[3]/div[1]/table/tbody/tr[2]/td[3]/strong")).getText();
        int price1;
        int price2;
        int priceBas;
        price1 = Integer.parseInt(priceRandVeg1);
        price2 = Integer.parseInt(priceRandVeg2);
        priceBas = Integer.parseInt(priceBasket);
        int sum = price1 + price2;
        Assert.assertTrue(sum == priceBas);

//        Debug
//        System.out.println(price1);
//        System.out.println(price2);
//        System.out.println(sum);
//        System.out.println(priceBasket);
    }

}
