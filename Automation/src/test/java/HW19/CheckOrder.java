package HW19;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class CheckOrder {
    By placeOrderBtn = By.xpath("//*[@id=\"root\"]/div/div/div/div/button");
    By checkboxAgree = By.xpath("//*[@id=\"root\"]/div/div/div/div/input");
    By proceedBtn = By.xpath("//*[@id=\"root\"]/div/div/div/div/button");

    public void clickPlaceOrderBtn(){
        $(placeOrderBtn).click();

    }

    public void agreeCheckbox(){
        $(checkboxAgree).click();
    }

    public void clickProceedBtn(){
        $(proceedBtn).click();
    }

    public void order(){
        this.clickPlaceOrderBtn();
        this.agreeCheckbox();
        this.clickProceedBtn();
    }
}
