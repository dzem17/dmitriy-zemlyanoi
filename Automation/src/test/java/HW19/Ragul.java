package HW19;

import com.codeborne.selenide.Selenide;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Random;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;
import static org.testng.Assert.assertEquals;

public class Ragul extends ConditionsTest {

    Random rand = new Random();
    int vegetables1 = rand.nextInt(30) + 1;
    int vegetables2 = rand.nextInt(30) + 1;
    By randomVegetables1 = By.xpath("/html/body/div/div/div[1]/div/div[" + (vegetables1) + "]/h4");
    By randomVegetables2 = By.xpath("/html/body/div/div/div[1]/div/div[" + (vegetables2) + "]/h4");

    @Test
    public void checkPageTitleRandVegetables() {
        open("");
        String randomVeg1 = $(randomVegetables1).getText();
        String randomVeg2 = $(randomVegetables2).getText();
        System.out.println("Random vegetable 1 - " + randomVeg1);
        System.out.println("Random vegetable 2 - " + randomVeg2);

    }

    @Test
    public void addBasket() {
        open("");
        new AddBasket()
                .randVegetables1()
                .randVegetables2()
                .openBasket()
                .checkText();
    }

    @Test
    public void addBuy() {
        open("");
        new AddBasket()
                .randVegetables1()
                .randVegetables2()
                .openBasket()
                .checkoutBtn();
        String title = "GreenKart - veg and fruits kart";
        Selenide.title();
        assertEquals(Selenide.title(), title);

    }

    @Test
    public void checkPromo() {
        open("");
        new AddBasket()
                .randVegetables1()
                .randVegetables2()
                .openBasket()
                .checkoutBtn();
        sleep(5000);
        new CheckPromoInvalidCode()
                .promo("corona");

    }

    @Test
    public void checkOrder() {
        open("");
        new AddBasket()
                .randVegetables1()
                .randVegetables2()
                .openBasket()
                .checkoutBtn();
        sleep(3000);
        new CheckOrder()
                .order();
        sleep(2000);
        $("[class=\"wrapperTwo\"]");
        $(By.xpath("//*[@id=\"root\"]/div/div/div/div/span")).shouldHave(text("Thank you, your order has been placed successfully "));

    }

    @Test
    public void checkinputFind() {
        open("");
        $(By.xpath("//*[@id=\"root\"]/div/header/div/div[2]/form/input")).setValue("meat");
        $(By.xpath("//*[@id=\"root\"]/div/div[1]/div/div/h2")).getText().contains("Sorry, no products matched your search!");

    }

    @Test
    public void checkPrice() {
         open("");
        new AddBasket()
                .sumRandVeg();

    }

}
