package hw1Lesson13;

import java.util.Scanner;



class ValidPhone {
    public static void main(String[] args) {
        String phone;
        Scanner input = new Scanner(System.in);  // Create a Scanner object
        do{
            System.out.println("Please enter your phone number in the format +38 XXX XXXX XXX");
            phone = input.nextLine();
        }while (!phone.matches("^\\+(?:[0-9] ?){6,14}[0-9]$"));
        System.out.println("----------Success----------");
        }

}

