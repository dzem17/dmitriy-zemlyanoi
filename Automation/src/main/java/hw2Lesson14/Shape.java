package hw2Lesson14;

public abstract class Shape {
    abstract public double CalculatePerimeter();
    abstract public double CalculateArea();
}
