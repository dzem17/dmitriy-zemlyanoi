package hw2Lesson14;

public class Rectangle extends Shape {
    private double width;
    private double length;

    Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }

    @Override
    public double CalculatePerimeter() {
        return 2 * (width + length);
    }

    @Override
    public double CalculateArea() {
        return width * length;
    }
}
