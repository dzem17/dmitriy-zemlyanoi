package hw2Lesson14;

public class Circle extends Shape {

    private final double PI = Math.PI;
    private double diameter;

       public Circle(double diameter) { // конструктор
        this.diameter = diameter;
    }

    @Override
    public double CalculatePerimeter(){
        return PI * diameter;
    }

    @Override
    public double CalculateArea() {
        double radius = diameter / 2.0;
        return PI * radius * radius;
    }
}
