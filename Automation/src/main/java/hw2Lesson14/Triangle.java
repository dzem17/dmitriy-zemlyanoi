package hw2Lesson14;

public class Triangle extends Shape {
    private double sideA;
    private double sideB;
    private double sideC;

    Triangle(double sideA, double sideB, double sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    @Override
    public double CalculatePerimeter() {
        return sideA + sideB + sideC;
    }

    @Override
    public double CalculateArea() {
        double hp = (sideA + sideB + sideC) / 2;
        return Math.sqrt(hp * (hp - sideA) * (hp - sideB) * (hp - sideC));
    }
}
