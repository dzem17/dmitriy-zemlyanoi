package hw2Lesson14;

import java.util.Scanner;

enum Figure {
    Rectangle, Square, Triangle, Circle
}

public class Calculator {

    public static void main(String[] args) {

        
        System.out.println("\n" + "Please choose one of the listed figures: Rectangle, Square, Triangle, Circle");

        Scanner input = new Scanner(System.in);
        String figure = input.nextLine();
        System.out.println("Your choose: " + figure);


        if (figure.equals(Figure.Rectangle.name()))
            rectangleCase();
        else if (figure.equals(Figure.Square.name()))
            squareCase();
                else if (figure.equals(Figure.Triangle.name()))
            triangleCase();
        else if (figure.equals(Figure.Circle.name()))
            circleCase();
        else {
            System.out.println("This figure is not listed");

        }

    }


    private static void rectangleCase() {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter width");
        double width = input.nextDouble();
        System.out.println("Enter length");
        double lenght = input.nextDouble();
        Rectangle rect = new Rectangle(width, lenght);
        System.out.println("The perimeter is: " + Math.ceil(rect.CalculatePerimeter()) + ", the area is: " + Math.ceil(rect.CalculateArea()));
        calculateTriangleSides(width, lenght);
    }

    private static void squareCase() {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter square side's length");
        double side = input.nextDouble();
        Square square = new Square(side);
        System.out.println("The perimeter is: " + Math.ceil(square.CalculatePerimeter()) + ", the area is: " + Math.ceil(square.CalculateArea()));
        calculateTriangleSides(side, side);
    }

    private static void triangleCase() {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter side a");
        double a = input.nextDouble();
        System.out.println("Enter side b");
        double b = input.nextDouble();
        System.out.println("Enter side c");
        double c = input.nextDouble();
        Triangle triangle = new Triangle(a, b, c);
        System.out.println("The perimeter is: " + Math.ceil(triangle.CalculatePerimeter()) + ", the area is: " + Math.ceil(triangle.CalculateArea()));
    }

    private static void circleCase() {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the diameter of the circle");
        double diameter = input.nextDouble();
        Circle circle = new Circle(diameter);
        System.out.println("The perimeter is: " + Math.ceil(circle.CalculatePerimeter()) + ", the area is: " + Math.ceil(circle.CalculateArea()));
    }



    private static void calculateTriangleSides(double a, double b) {
        double sqrC = (a * a) + (b * b);
        double c = Math.sqrt(sqrC);
        System.out.format("The side A of the triangle equals to " + a + ", side B equals to " + b + " and side C equals to %.1f%n", c);
    }
}



